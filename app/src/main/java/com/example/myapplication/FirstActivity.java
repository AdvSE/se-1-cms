package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Button buttonOne = findViewById(R.id.register);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i1 = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(i1);
            }
        });

        Button buttontwo = findViewById(R.id.login);
        buttontwo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {

                Intent i2 = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i2);
            }
        });
        Button buttonthree = findViewById(R.id.button2);
        buttonthree.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {

                Intent i2 = new Intent(getApplicationContext(), AboutAppActivity.class);
                startActivity(i2);
            }
        });

    }
}
