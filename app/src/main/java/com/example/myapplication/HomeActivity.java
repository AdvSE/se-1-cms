package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Button buttonc = findViewById(R.id.logout);
        buttonc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i1 = new Intent(getApplicationContext(), FirstActivity.class);
                startActivity(i1);
            }
        });
        final String utaIdValue = getIntent().getStringExtra("utaId");
        Button buttonOne = findViewById(R.id.dept_info);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i1 = new Intent(getApplicationContext(), DepartmentinfoActivity.class);
                startActivity(i1);
            }
        });

        Button buttonTwo = findViewById(R.id.create);
        if(utaIdValue.equals("1001759482")) {
            buttonTwo.setVisibility(View.GONE);
        }
        buttonTwo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                Intent i2 = new Intent(HomeActivity.this, MainActivity.class);
                i2.putExtra("utaId", utaIdValue);
                startActivity(i2);
            }
        });

        Button buttonThree = findViewById(R.id.view);

        buttonThree.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v2) {
                Intent i3 = new Intent(getApplicationContext(), ViewActivity.class);
                i3.putExtra("utaId", utaIdValue);
                startActivity(i3);
            }
        });
    }
}
