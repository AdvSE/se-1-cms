package com.example.myapplication;

import com.example.myapplication.pojo.Student;
import com.fasterxml.jackson.databind.JsonNode;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CMSService {

    @POST("/users/validateUser")
    Call<JsonNode> validateUser(@Body JsonNode utaIdPassword);

    @POST("/users/signup")
    Call<JsonNode> createUser(@Body Student student);

    @POST("/users/{utaId}/department/{departmentName}/createTicket")
    Call<JsonNode> createTicket(@Body JsonNode description, @Path("utaId") String utaId, @Path("departmentName") String departmentName);

    @GET("/users/{utaId}/viewTickets")
    Call<JsonNode> viewTickets(@Path("utaId") String utaId);

    @POST("/users/updateTicket/{ticketId}/{status}")
    Call<JsonNode> updateTicket(@Path("ticketId") String ticketId, @Path("status") String status);
}
