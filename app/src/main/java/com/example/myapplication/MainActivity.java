package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.asynctasks.CreateTicket;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
    EditText utaId;
    TextView description;
    Spinner department;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonc = findViewById(R.id.logout);
        buttonc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i1 = new Intent(getApplicationContext(), FirstActivity.class);
                startActivity(i1);
            }
        });
        department = findViewById(R.id.department);
        utaId = findViewById(R.id.UTAID);
        utaId.setText(getIntent().getStringExtra("utaId"));
        description = findViewById(R.id.Description);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.departments,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        department.setAdapter(adapter);
        submit = findViewById(R.id.button);
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                try {
                    String result = new CreateTicket().execute(description.getText().toString(), utaId.getText().toString(), department.getSelectedItem().toString()).get();
                    setMessage(result);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
//        spinner.setOnItemClickListener(this);
    }

    private void setMessage(String message) {
        Toast t = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        t.show();

        if(message.contains("Ticket is created")){
            Intent i = new Intent(MainActivity.this, ViewActivity.class);
            i.putExtra("utaId", utaId.getText().toString());
            startActivity(i);
            this.finish();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
