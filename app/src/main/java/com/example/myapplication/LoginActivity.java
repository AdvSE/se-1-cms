package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.asynctasks.ValidateUser;

import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {
    EditText utaId;
    EditText password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupUI();
        setupListeners();
    }

    private void setupUI() {
        utaId = findViewById(R.id.utaid);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
    }

    private void setupListeners() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUsername();
            }
        });
    }

    void checkUsername() {
        boolean isValid = true;
        if (isEmpty(utaId)) {
            utaId.setError("You must enter utaId to login!");
            isValid = false;
        }


        if (isEmpty(password)) {
            password.setError("You must enter password to login!");
            isValid = false;
        }



        if (isValid) {
            String utaIdValue = utaId.getText().toString();
            String passwordValue = password.getText().toString();

            Boolean isValidCredentials = false;

            try {
                isValidCredentials = new ValidateUser().execute(utaIdValue, passwordValue).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

            if (isValidCredentials) {
                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                i.putExtra("utaId", utaIdValue);
                startActivity(i);
                this.finish();
            } else {
                Toast t = Toast.makeText(this, "Wrong utaId or password!", Toast.LENGTH_SHORT);
                t.show();
            }
        }
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

}
