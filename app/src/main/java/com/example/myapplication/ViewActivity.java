package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.asynctasks.UpdateTicket;
import com.example.myapplication.asynctasks.ViewTickets;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import java.util.concurrent.ExecutionException;

public class ViewActivity extends AppCompatActivity {
    Integer clicked_id;
    String new_status;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        Button buttonc = findViewById(R.id.logout);
        buttonc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i1 = new Intent(getApplicationContext(), FirstActivity.class);
                startActivity(i1);
            }
        });

        TableLayout tableLayout = (TableLayout) findViewById(R.id.table);
        TableRow headingRow = new TableRow(this);

        TextView idHeading = new TextView(this);
        idHeading.setText("Ticket Id");
        headingRow.addView(idHeading);

        TextView departmentHeading = new TextView(this);
        departmentHeading.setText("Depart");
        headingRow.addView(departmentHeading);

        TextView descriptionHeading = new TextView(this);
        descriptionHeading.setText("Descrtiption");
        headingRow.addView(descriptionHeading);

        TextView statusHeading = new TextView(this);
        statusHeading.setText("Status");
        headingRow.addView(statusHeading);
        tableLayout.addView(headingRow);

        final String utaIdValue = getIntent().getStringExtra("utaId");
        ArrayNode tickets = new ArrayNode(JsonNodeFactory.instance);

        try {
            tickets = new ViewTickets().execute(utaIdValue).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        for (JsonNode jsonNode : tickets) {
            String id = jsonNode.get("id").asText();
            String description = jsonNode.get("description").asText();
            String statusId = jsonNode.get("status").asText();

            TableRow row = new TableRow(this);

            TextView idVal = new TextView(this);
            idVal.setText(id);
            row.addView(idVal);

            TextView departmentVal= new TextView(this);
            departmentVal.setText("Housing");
            row.addView(departmentVal);

            TextView descriptionVal = new TextView(this);
            descriptionVal.setText(description);
            row.addView(descriptionVal);

            Spinner statusVal = new Spinner(this);
            String[] items = new String[]{"Created", "In Progress", "Completed"};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

            statusVal.setAdapter(adapter);
            statusVal.setSelection(Integer.parseInt(statusId));
            statusVal.setId(Integer.parseInt(id));
            statusVal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    clicked_id = parent.getId();
                    new_status = (String) parent.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            row.addView(statusVal);
            tableLayout.addView(row);
        }

        submit = findViewById(R.id.submit);
        if(!utaIdValue.equals("1001759482")) {
            submit.setVisibility(View.GONE);
        }
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
                try {
                    String result = new UpdateTicket().execute(clicked_id.toString(), new_status).get();
                    setMessage(result);
                    Intent i2 = new Intent(ViewActivity.this, ViewActivity.class);
                    i2.putExtra("utaId", utaIdValue);
                    startActivity(i2);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setMessage(String message) {
        Toast t = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        t.show();
    }
}
