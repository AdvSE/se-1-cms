package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.asynctasks.CreateUser;
import com.example.myapplication.pojo.Student;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity {
    EditText name;
    EditText email;
    EditText password;
    EditText utaid;
    EditText major;
    EditText aptname;
    EditText aptno;
    Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        utaid = findViewById(R.id.utaid);
        major = findViewById(R.id.major);
        aptname = findViewById(R.id.aptname);
        aptno = findViewById(R.id.aptno);
        signup = findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkDataEntered()){
                    Student student = new Student();
                    student.setName(name.getText().toString());
                    student.setEmailId(email.getText().toString());
                    student.setPassword(password.getText().toString());
                    student.setUtaId(utaid.getText().toString());
                    student.setMajor(major.getText().toString());
                    student.setApartmentName(aptname.getText().toString());
                    student.setApartmentNumber(aptno.getText().toString());

                    try {
                        String result = new CreateUser().execute(student).get();
                        setMessage(result);
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    private void setMessage(String message) {
        Toast t = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        t.show();
        Button button1 = findViewById(R.id.signup);

                Intent i1 = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i1);

    }

    private boolean checkDataEntered() {
        boolean validData = true;

        if (isEmpty(name)) {
            validData = false;
            Toast t = Toast.makeText(this, "You must enter name to register!", Toast.LENGTH_SHORT);
            t.show();
        }
        else if (isEmpty(email)) {
            validData = false;
            Toast t = Toast.makeText(this, "You must enter email to register!", Toast.LENGTH_SHORT);
            t.show();
        }
        else if (isEmpty(password)) {
            validData = false;
            Toast t = Toast.makeText(this, "You must enter password to register!", Toast.LENGTH_SHORT);
            t.show();
        }
        else if (isEmpty(utaid)) {
            validData = false;
            Toast t = Toast.makeText(this, "You must enter UTA Id to register!", Toast.LENGTH_SHORT);
            t.show();
        }
        else if (isEmpty(major)) {
            validData = false;
            Toast t = Toast.makeText(this, "You must enter major to register!", Toast.LENGTH_SHORT);
            t.show();
        }
        else if (!isEmail(email)) {
            validData = false;
            email.setError("Enter valid email!");
        }
        else if (!ispassword(password)) {
            validData = false;
            password.setError("Enter valid password.The password must be at least 8 characters long and include a number, lowercase letter, uppercase letter and special character");
        }

        return validData;
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    boolean ispassword(EditText text) {
        CharSequence password = text.getText().toString();

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

}
