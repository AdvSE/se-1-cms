package com.example.myapplication.asynctasks;

import android.os.AsyncTask;

import com.example.myapplication.CMSService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ViewTickets extends AsyncTask<String, Void, ArrayNode > {
    @Override
    protected ArrayNode doInBackground(String... utaIds){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        CMSService service = retrofit.create(CMSService.class);

        try {
            Response<JsonNode> result = service.viewTickets(utaIds[0]).execute();
            return (ArrayNode) result.body();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayNode(JsonNodeFactory.instance);
    }
}
