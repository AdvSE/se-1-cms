package com.example.myapplication.asynctasks;

import android.os.AsyncTask;

import com.example.myapplication.CMSService;
import com.example.myapplication.pojo.Student;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class CreateUser extends AsyncTask<Student, Void, String> {
    @Override
    protected String doInBackground(Student... students) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        CMSService service = retrofit.create(CMSService.class);

        try {
            Response<JsonNode> result = service.createUser(students[0]).execute();
            return result.body().findValue("message").toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "failed to create user";
    }
}
