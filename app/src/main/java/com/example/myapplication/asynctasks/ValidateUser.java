package com.example.myapplication.asynctasks;

import android.os.AsyncTask;

import com.example.myapplication.CMSService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ValidateUser extends AsyncTask<String, Void, Boolean> {
    @Override
    protected Boolean doInBackground(String... credentials) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        CMSService service = retrofit.create(CMSService.class);

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.createObjectNode();
            ((ObjectNode) rootNode).put("utaId", credentials[0]);
            ((ObjectNode) rootNode).put("password", credentials[1]);
            Response<JsonNode> result = service.validateUser(rootNode).execute();

            if(result.body().get("message").toString().contains("User credentials are valid"))
                return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
