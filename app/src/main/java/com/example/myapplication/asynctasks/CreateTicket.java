package com.example.myapplication.asynctasks;

import android.os.AsyncTask;

import com.example.myapplication.CMSService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class CreateTicket extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        CMSService service = retrofit.create(CMSService.class);

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.createObjectNode();
            ((ObjectNode) rootNode).put("description", params[0]);
            Response<JsonNode> result = service.createTicket(rootNode, params[1], params[2]).execute();
            return result.body().get("message").toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Failed to create ticket";
    }
}
