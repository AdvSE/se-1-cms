package com.example.myapplication.asynctasks;

import android.os.AsyncTask;

import com.example.myapplication.CMSService;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class UpdateTicket extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... params){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        CMSService service = retrofit.create(CMSService.class);

        try {
            Response<JsonNode> result = service.updateTicket(params[0], params[1]).execute();
            return result.body().get("message").toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Failed to update ticket";
    }
}
